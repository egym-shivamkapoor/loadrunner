//   *****************************************************************************************************************************************
//   ****   PLEASE NOTE: This is a READ-ONLY representation of the actual script. For editing please press the "Develop Script" button.   ****
//   *****************************************************************************************************************************************

Action()
{
	truclient_step("1", "Navigate to 'https://ibs401.intratec...ualitrain_beta/'", "snapshot=Action_1.inf");
	truclient_step("2", "Type Test Bea Admin in Benutzername textbox", "snapshot=Action_2.inf");
	truclient_step("3", "Click on Kennwort passwordbox", "snapshot=Action_3.inf");
	truclient_step("4", "Type ********* in Kennwort passwordbox", "snapshot=Action_4.inf");
	truclient_step("5", "Click on Login button", "snapshot=Action_5.inf");
	truclient_step("6", "Mouse Over", "snapshot=Action_6.inf");
	{
		truclient_step("6.1", "Move mouse over focusable (1)", "snapshot=Action_6.1.inf");
	}
	truclient_step("7", "Mouse down on Kundenverwaltung", "snapshot=Action_7.inf");
	truclient_step("8", "Click on Kundenverwaltung", "snapshot=Action_8.inf");
	truclient_step("10", "Mouse Over", "snapshot=Action_10.inf");
	{
		truclient_step("10.2", "Move mouse over Kundenverwaltung", "snapshot=Action_10.2.inf");
	}
	truclient_step("11", "Mouse down on Kundenverwaltung", "snapshot=Action_11.inf");
	truclient_step("12", "Click on Kundenverwaltung", "snapshot=Action_12.inf");
	truclient_step("15", "Click on Vorname textbox", "snapshot=Action_15.inf");
	truclient_step("16", "Type TC.getParam('Vorname') in Vorname textbox", "snapshot=Action_16.inf");
	truclient_step("17", "Click on Nachname textbox", "snapshot=Action_17.inf");
	truclient_step("18", "Click on Nachname textbox", "snapshot=Action_18.inf");
	truclient_step("19", "Click on Nachname textbox", "snapshot=Action_19.inf");
	truclient_step("20", "Type TC.getParam('Nachname') in Nachname textbox", "snapshot=Action_20.inf");
	truclient_step("21", "Click on Suchen button", "snapshot=Action_21.inf");
	truclient_step("22", "Click on Zusatzdaten", "snapshot=Action_22.inf");
	truclient_step("23", "Click on Firmenvertrag checkbox", "snapshot=Action_23.inf");
	truclient_step("24", "Click on Weitere Daten button", "snapshot=Action_24.inf");
	truclient_step("27", "Click on Klicken Sie hier, um... image", "snapshot=Action_27.inf");
	truclient_step("28", "Set datepicker (1) datepicker to 20", "snapshot=Action_28.inf");
	truclient_step("29", "Click on Weitere Daten button", "snapshot=Action_29.inf");
	truclient_step("30", "Click on Klicken Sie hier, um... image", "snapshot=Action_30.inf");
	truclient_step("31", "Click on Ende textbox", "snapshot=Action_31.inf");
	truclient_step("32", "Select TC.getParam('Month') from Monat ausw�hlen listbox", "snapshot=Action_32.inf");
	truclient_step("33", "Select TC.getParam('Year') from Jahr ausw�hlen listbox", "snapshot=Action_33.inf");
	truclient_step("34", "Set datepicker (1) datepicker to TC.getParam('Date')", "snapshot=Action_34.inf");
	truclient_step("35", "Click on speichern button", "snapshot=Action_35.inf");
	truclient_step("37", "Scroll on Pers�nlicher Bereich...", "snapshot=Action_37.inf");
	truclient_step("39", "Click on speichern button", "snapshot=Action_39.inf");
	truclient_step("40", "Press OK in alert dialog", "snapshot=Action_40.inf");

	return 0;
}
